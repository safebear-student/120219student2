/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8260869565217391, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "22 LaunchSimpleRisk"], "isController": false}, {"data": [1.0, 500, 1500, "30 CreateRiskandSubmitWithoutAttachment"], "isController": false}, {"data": [1.0, 500, 1500, "37 Logout"], "isController": false}, {"data": [1.0, 500, 1500, "36 Logout"], "isController": false}, {"data": [1.0, 500, 1500, "CreateRiskandSubmitWithoutAttachment"], "isController": true}, {"data": [1.0, 500, 1500, "LaunchSimpleRisk"], "isController": true}, {"data": [1.0, 500, 1500, "23 LogintoSimpleRisk-1"], "isController": false}, {"data": [1.0, 500, 1500, "35 Logout"], "isController": false}, {"data": [1.0, 500, 1500, "23 LogintoSimpleRisk-2"], "isController": false}, {"data": [1.0, 500, 1500, "26 GotoRiskManagement"], "isController": false}, {"data": [0.0, 500, 1500, "23 LogintoSimpleRisk"], "isController": false}, {"data": [0.0, 500, 1500, "Submit Risk Without Attachment"], "isController": true}, {"data": [0.0, 500, 1500, "23 LogintoSimpleRisk-0"], "isController": false}, {"data": [1.0, 500, 1500, "24 LogintoSimpleRisk-1"], "isController": false}, {"data": [1.0, 500, 1500, "31 CreateRiskandSubmitWithoutAttachment"], "isController": false}, {"data": [1.0, 500, 1500, "36 Logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "24 LogintoSimpleRisk"], "isController": false}, {"data": [1.0, 500, 1500, "24 LogintoSimpleRisk-0"], "isController": false}, {"data": [1.0, 500, 1500, "36 Logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "Logout"], "isController": true}, {"data": [1.0, 500, 1500, "GotoRiskManagement"], "isController": true}, {"data": [0.0, 500, 1500, "LogintoSimpleRisk"], "isController": true}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 17, 0, 0.0, 735.5882352941177, 1, 5995, 5967.8, 5995.0, 5995.0, 0.2557467805993501, 1.0790345700069204, 0.21248079548531715], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["22 LaunchSimpleRisk", 1, 0, 0.0, 85.0, 85, 85, 85.0, 85.0, 85.0, 11.76470588235294, 16.47518382352941, 3.825827205882353], "isController": false}, {"data": ["30 CreateRiskandSubmitWithoutAttachment", 1, 0, 0.0, 104.0, 104, 104, 104.0, 104.0, 104.0, 9.615384615384617, 65.32639723557692, 56.73452524038462], "isController": false}, {"data": ["37 Logout", 1, 0, 0.0, 22.0, 22, 22, 22.0, 22.0, 22.0, 45.45454545454545, 60.546875, 18.865411931818183], "isController": false}, {"data": ["36 Logout", 1, 0, 0.0, 58.0, 58, 58, 58.0, 58.0, 58.0, 17.241379310344826, 32.56330818965517, 14.6484375], "isController": false}, {"data": ["CreateRiskandSubmitWithoutAttachment", 1, 0, 0.0, 174.0, 174, 174, 174.0, 174.0, 174.0, 5.747126436781609, 194.60533405172416, 36.194549209770116], "isController": true}, {"data": ["LaunchSimpleRisk", 1, 0, 0.0, 85.0, 85, 85, 85.0, 85.0, 85.0, 11.76470588235294, 16.47518382352941, 3.825827205882353], "isController": true}, {"data": ["23 LogintoSimpleRisk-1", 1, 0, 0.0, 3.0, 3, 3, 3.0, 3.0, 3.0, 333.3333333333333, 189.453125, 153.3203125], "isController": false}, {"data": ["35 Logout", 1, 0, 0.0, 30.0, 30, 30, 30.0, 30.0, 30.0, 33.333333333333336, 223.92578125, 14.811197916666668], "isController": false}, {"data": ["23 LogintoSimpleRisk-2", 1, 0, 0.0, 29.0, 29, 29, 29.0, 29.0, 29.0, 34.48275862068965, 97.01643318965517, 15.894396551724137], "isController": false}, {"data": ["26 GotoRiskManagement", 1, 0, 0.0, 29.0, 29, 29, 29.0, 29.0, 29.0, 34.48275862068965, 231.6136853448276, 14.917834051724137], "isController": false}, {"data": ["23 LogintoSimpleRisk", 1, 0, 0.0, 5995.0, 5995, 5995, 5995.0, 5995.0, 5995.0, 0.16680567139282737, 0.8819198290241868, 0.23847998331943285], "isController": false}, {"data": ["Submit Risk Without Attachment", 1, 0, 0.0, 6425.0, 6425, 6425, 6425.0, 6425.0, 6425.0, 0.1556420233463035, 9.42956590466926, 1.715102140077821], "isController": true}, {"data": ["23 LogintoSimpleRisk-0", 1, 0, 0.0, 5961.0, 5961, 5961, 5961.0, 5961.0, 5961.0, 0.16775708773695688, 0.3196231232175809, 0.08535297139741653], "isController": false}, {"data": ["24 LogintoSimpleRisk-1", 1, 0, 0.0, 30.0, 30, 30, 30.0, 30.0, 30.0, 33.333333333333336, 93.78255208333334, 13.76953125], "isController": false}, {"data": ["31 CreateRiskandSubmitWithoutAttachment", 1, 0, 0.0, 70.0, 70, 70, 70.0, 70.0, 70.0, 14.285714285714285, 386.67689732142856, 5.678013392857142], "isController": false}, {"data": ["36 Logout-0", 1, 0, 0.0, 35.0, 35, 35, 35.0, 35.0, 35.0, 28.57142857142857, 15.904017857142856, 12.416294642857142], "isController": false}, {"data": ["Debug Sampler", 1, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1000.0, 583.984375, 0.0], "isController": false}, {"data": ["24 LogintoSimpleRisk", 1, 0, 0.0, 32.0, 32, 32, 32.0, 32.0, 32.0, 31.25, 105.65185546875, 25.787353515625], "isController": false}, {"data": ["24 LogintoSimpleRisk-0", 1, 0, 0.0, 1.0, 1, 1, 1.0, 1.0, 1.0, 1000.0, 567.3828125, 412.109375], "isController": false}, {"data": ["36 Logout-1", 1, 0, 0.0, 20.0, 20, 20, 20.0, 20.0, 20.0, 50.0, 66.6015625, 20.751953125], "isController": false}, {"data": ["Logout", 1, 0, 0.0, 110.0, 110, 110, 110.0, 110.0, 110.0, 9.09090909090909, 90.34978693181819, 15.536221590909092], "isController": true}, {"data": ["GotoRiskManagement", 1, 0, 0.0, 29.0, 29, 29, 29.0, 29.0, 29.0, 34.48275862068965, 231.6136853448276, 14.917834051724137], "isController": true}, {"data": ["LogintoSimpleRisk", 1, 0, 0.0, 6027.0, 6027, 6027, 6027.0, 6027.0, 6027.0, 0.16592002654720425, 1.4381896051103369, 0.3741302161108346], "isController": true}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 17, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
